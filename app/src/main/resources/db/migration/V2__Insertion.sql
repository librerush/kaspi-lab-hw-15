INSERT INTO manufacturer VALUES (1, 'Milka', 'milka.com');
INSERT INTO manufacturer VALUES (2, 'Rakhat', 'rakhat.kz');
INSERT INTO manufacturer VALUES (3, 'Foodmaster', 'foodmaster.kz');

INSERT INTO product VALUES (1, 'Oreo', 250, 1);
INSERT INTO product VALUES (2, 'Kazakhstan chocolate', 300, 2);
INSERT INTO product VALUES (3, 'Kefir', 200, 3);

INSERT INTO client VALUES (1, 'Rebecca', 'Dostyk Street');
INSERT INTO client VALUES (2, 'Aybek', 'Satpayev Street');
INSERT INTO client VALUES (3, 'Moldyr', 'Abay avenue');

INSERT INTO "order" VALUES (1, 1, '2021-03-20');
INSERT INTO "order" VALUES (2, 2, '2021-03-22');
INSERT INTO "order" VALUES (3, 3, '2021-03-25');