package hw15.dao;

import hw15.Product;

import java.util.List;
import java.util.Optional;

public class ProductDao extends BaseDao<Product, Integer> {

    @Override
    public void save(Product product) {
        getSession().save(product);
    }

    @Override
    public void update(Product product) {
        getSession().update(product);
    }

    @Override
    public void delete(Product product) {
        getSession().delete(product);
    }

    @Override
    public Optional<Product> findById(Integer integer) {
        Product product = getSession().get(Product.class, integer);
        return (product == null) ? Optional.empty() : Optional.of(product);
    }

    @Override
    public List<Product> findAll() {
        return getSession().createQuery("from Product ").list();
    }
}
