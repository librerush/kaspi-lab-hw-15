package hw15.dao;

import hw15.SessionUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

public abstract class BaseDao<Cls, Id> implements Dao<Cls, Id> {
    private Session session;
    private Transaction transaction;

    public Session openSession() {
        session = SessionUtil.getSessionFactory().openSession();
        transaction = session.beginTransaction();
        return session;
    }

    public void closeSession() {
        transaction.commit();
        session.close();
    }

    public Session getSession() {
        return session;
    }
}
