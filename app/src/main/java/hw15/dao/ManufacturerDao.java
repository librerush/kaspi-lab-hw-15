package hw15.dao;

import hw15.Manufacturer;

import java.util.List;
import java.util.Optional;

public class ManufacturerDao extends BaseDao<Manufacturer, Integer> {

    @Override
    public void save(Manufacturer manufacturer) {
        getSession().save(manufacturer);
    }

    @Override
    public void update(Manufacturer manufacturer) {
        getSession().update(manufacturer);
    }

    @Override
    public void delete(Manufacturer manufacturer) {
        getSession().delete(manufacturer);
    }

    @Override
    public Optional<Manufacturer> findById(Integer integer) {
        Manufacturer manufacturer = getSession().get(Manufacturer.class, integer);
        if (manufacturer != null) {
            return Optional.of(manufacturer);
        }
        return Optional.empty();
    }

    @Override
    public List<Manufacturer> findAll() {
        return getSession().createQuery("from Manufacturer ").list();
    }
}
