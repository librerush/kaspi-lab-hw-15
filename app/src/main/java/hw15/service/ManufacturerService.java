package hw15.service;

import hw15.Manufacturer;
import hw15.dao.Dao;
import hw15.dao.ManufacturerDao;

import java.util.List;
import java.util.Optional;

public class ManufacturerService {
    private static Dao<Manufacturer, Integer> manufacturerDao;

    public ManufacturerService() {
        manufacturerDao = new ManufacturerDao();
    }

    public void save(Manufacturer manufacturer) {
        manufacturerDao.openSession();
        manufacturerDao.save(manufacturer);
        manufacturerDao.closeSession();
    }

    public void update(Manufacturer manufacturer) {
        manufacturerDao.openSession();
        manufacturerDao.update(manufacturer);
        manufacturerDao.closeSession();
    }

    public void delete(Manufacturer manufacturer) {
        manufacturerDao.openSession();
        manufacturerDao.delete(manufacturer);
        manufacturerDao.closeSession();
    }

    public Manufacturer findById(Integer id) {
        manufacturerDao.openSession();
        Optional<Manufacturer> manufacturer = manufacturerDao.findById(id);
        manufacturerDao.closeSession();
        return manufacturer.get();
    }

    public List<Manufacturer> findAll() {
        manufacturerDao.openSession();
        List manufacturers = manufacturerDao.findAll();
        manufacturerDao.closeSession();
        return manufacturers;
    }
}
