package hw15.service;

import hw15.Order;
import hw15.dao.OrderDao;
import hw15.dao.Dao;

import java.util.List;
import java.util.Optional;

public class OrderService {
    private static Dao<Order, Integer> orderDao;

    public OrderService() {
        orderDao = new OrderDao();
    }

    public void save(Order client) {
        orderDao.openSession();
        orderDao.save(client);
        orderDao.closeSession();
    }

    public void update(Order client) {
        orderDao.openSession();
        orderDao.update(client);
        orderDao.closeSession();
    }

    public void delete(Order client) {
        orderDao.openSession();
        orderDao.delete(client);
        orderDao.closeSession();
    }

    public Order findById(Integer id) {
        orderDao.openSession();
        Optional<Order> client = orderDao.findById(id);
        orderDao.closeSession();
        return client.get();
    }

    public List<Order> findAll() {
        orderDao.openSession();
        List clients = orderDao.findAll();
        orderDao.closeSession();
        return clients;
    }
}
